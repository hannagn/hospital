package Core;
import Core.Exception.RemoveException;
import java.util.ArrayList;
import java.util.Objects;

/**
 * This is a class which represents the department of a hospital, the department consists of a name for the department,
 * a list of the employees at the hospital and a list of the patients admitted to the hospital
 */
public class Department {
    private String departmentName;
    private ArrayList<Employee> employees;
    private ArrayList<Patient> patients;

    /**
     * A constructor for Department objects that only requires department name as a parameter
     * @param departmentName the name of the Department
     */
    public Department(String departmentName) {
        this.departmentName = departmentName;
        this.employees = new ArrayList<>();
        this.patients = new ArrayList<>();
    }

    /**
     * A constructor for Department objects that requires all the class' attributes as parameters
     * @param departmentName the name of the Department
     * @param employees the list of employees in this Department
     * @param patients the list of patients in this Department
     */
    public Department(String departmentName, ArrayList<Employee> employees, ArrayList<Patient> patients) {
        this.departmentName = departmentName;
        this.employees = employees;
        this.patients = patients;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * A method which sets a new name of the Department
     * @param departmentName name of Department
     */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }
    public ArrayList<Employee> getEmployees(){
        return employees;
    }

    /**
     * A method who takes an object of type Employee and adds this object to the employee list in this Department, only if
     * the employee is not already in this list. If an already registered employee is attempted registered again, this method
     * throws an Illegal argument exception
     * @param employee the employee to be registered in Department list of employees
     */
    public void addEmployee(Employee employee){
        if(!(employees.contains(employee))) {
            employees.add(employee);
        }else {
            throw new IllegalArgumentException("Could not add this employee");
        }
    }

    public ArrayList<Patient> getPatients(){
        return patients;
    }

    /**
     * A method who takes an object of type Patient and adds this object to the patient list in this Department, only if
     * the patient is not already in this list. If an already registered patient is attempted registered again, this method
     * throws an Illegal argument exception
     * @param patient the patient to be registered in Department list of patients
     */
    public void addPatient(Patient patient){
        if(!(patients.contains(patient))) {
            patients.add(patient);
        }else{
            throw new IllegalArgumentException("Could not add this patient");
        }
    }

    /**
     * A method who takes in an object of type Person and removes that person from the Department list of either
     * employees or patients depending on which of the two types they are, else an exception is thrown
     * @param person the object of type Person to be removed from list in Department
     * @throws RemoveException an exception that is thrown if the Person object is neither employee nor patient
     */
    public void remove(Person person) throws RemoveException {
        if(person == null){
            throw new IllegalArgumentException();
        }
        if (person instanceof Employee && employees.contains(person)) {
            employees.remove(person);
        } else if (person instanceof Patient && patients.contains(person)) {
            patients.remove(person);
        } else {
            throw new RemoveException("This is neither a person of type patient nor employee");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return Objects.equals(departmentName, that.departmentName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(departmentName);
    }

    public String toString(){
        return "\nDepartment: " + departmentName +
                "\nEmployees: " + employees +
                "\nPatients: " + patients;
    }
}
