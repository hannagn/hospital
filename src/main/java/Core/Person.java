package Core;

/**
 * A super class which builds a basis for all the people at a hospital, such as patients and employees
 */
public abstract class Person {
    private String firstName;
    private String lastName;
    private String socialSecurityNumber;

    /**
     * The constructor for objects of type person, checks that all the attributes handed in as parameters is not null
     * @param firstName the first name of the person
     * @param lastName the last name of the person
     * @param socialSecurityNumber the social security number of the person
     */
    public Person(String firstName, String lastName, String socialSecurityNumber) {
        if(firstName != null && lastName != null && socialSecurityNumber != null) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.socialSecurityNumber = socialSecurityNumber;
        }
    }

    public String getFirstName() {
        return firstName;
    }

    /**
     * A method to set the first name of a person
     * @param firstName the first name of the person
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    /**
     * A method to set the last name of a person
     * @param lastName the last name of the person
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    /**
     * A method to set the social security number of a person
     * @param socialSecurityNumber the social security number of a person
     */
    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }
    public void getFullName(String firstName, String lastName){
        this.firstName = firstName + lastName;
    }

    @Override
    public String toString() {
        return  "\nFull name: " + firstName + " " + lastName +
                "\nSocialsecurity number: " + socialSecurityNumber;
    }
}
