package Core;

/**
 * A class that represents the patients at a hospital
 */
public class Patient extends Person implements Diagnosable{
    private String diagnosis = "";

    /**
     * A protected constructor for the objects of type patient, where this diagnosis is set
     * @param firstName the first name of the patient
     * @param lastName the last name of the patient
     * @param socialSecurityNumber the social security number of the patient
     */
    protected Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
        this.diagnosis = diagnosis;
    }

    protected String getDiagnosis(){
        return diagnosis;
    }

    /**
     * A method to set the diagnosis of the patient
     * @param diagnosis the diagnose of a patient
     */
    @Override
    public void setDiagnosis(String diagnosis){
        this.diagnosis = diagnosis;
    }

    @Override
    public String toString(){
        return "\nPatient: " + super.toString() +
                "\nDiagnose: " + diagnosis;
    }
}
