package Core;

import java.util.ArrayList;

/**
 * This class represents a simple hospital with a name, and also one or more Departments
 */
public class Hospital {
    private final String hospitalName;
    private ArrayList<Department> departments;

    /**
     * The constructor for the type Hospital
     * @param hospitalName the name of the hospital
     */
    public Hospital(String hospitalName) {
        this.hospitalName = hospitalName;
        this.departments = new ArrayList<>();
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public ArrayList<Department> getDepartments() {
        return departments;
    }

    /**
     * A method to add Departments to this hospital. If the department already exists in this hospital en exception is thrown,
     * else the new department is added to this hospital
     * @param department the department to be added
     */
    public void addDepartment(Department department){
        if(!(departments.contains(department))) {
            departments.add(department);
        }else{
            throw new IllegalArgumentException("Could not add this department");
        }
    }

    public String toString(){
        return "\n\nHospital: " + hospitalName +
                "\nDepartments: " + departments + "\n\n";
    }
}
