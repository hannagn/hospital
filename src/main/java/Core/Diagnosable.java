package Core;

/**
 * This class represents the diagnosis of a patient
 */
public interface Diagnosable {

    /**
     * This methods sets a patients diagnosis, only employees of types general practitioner and surgeon is able to set diagnose
     * @param diagnosis the diagnose of a patient
     */
    void setDiagnosis(String diagnosis);
}
