package Core.Exception;

/**
 * This class contains a specialized exception method who removes objects of type Patient or Employee
 */
public class RemoveException extends Exception{
    private static final long serialVersionUID = 1L;

    public RemoveException(String message) {
        super(message);
    }
}
