package HealthPersonell;
import Core.Employee;

/**
 * This class represents all nurses working at this hospital
 */
public class Nurse extends Employee {
    public Nurse(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
