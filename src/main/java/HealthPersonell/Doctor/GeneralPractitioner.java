package HealthPersonell.Doctor;
import Core.Patient;

/**
 * This is a class representing the general practitioners working at a hospital
 */
public class GeneralPractitioner extends Doctor {
    public GeneralPractitioner(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * A method to set diagnosis on a patient
     * @param patient the patient who receives the diagnosis
     * @param diagnosis the diagnosis of this patient
     */
    @Override
    public void setDiagnosis(Patient patient, String diagnosis) {
    }
}
