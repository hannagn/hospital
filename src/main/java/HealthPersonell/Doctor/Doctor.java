package HealthPersonell.Doctor;
import Core.Employee;
import Core.Patient;

/**
 * A class that represents all the types of doctors at a hospital
 */
public abstract class Doctor extends Employee {
    protected Doctor(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * A method to set diagnosis on a patient
     * @param patient the patient who receives the diagnosis
     * @param diagnosis the diagnosis of this patient
     */
    public abstract void setDiagnosis(Patient patient, String diagnosis);
}
