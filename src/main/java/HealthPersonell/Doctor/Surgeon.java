package HealthPersonell.Doctor;
import Core.Patient;

/**
 * This class represents all surgeons working at this hospital
 */
public class Surgeon extends Doctor {
    public Surgeon(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * A method to set diagnosis on a patient
     * @param patient the patient who receives the diagnosis
     * @param diagnosis the diagnosis of this patient
     */
    @Override
    public void setDiagnosis(Patient patient, String diagnosis) {
    }
}
